package com.builder;

import com.config.VariableSetter;
import com.service.lowestattendance.AttendanceDriver;
import com.service.loadprototohbase.BuildingHbaseDriver;
import com.service.loadprototohbase.EmployeeHbaseDriver;
import com.service.populatecafeteriacode.EmployeePostProcessorDriver;
import com.service.writeprototohdfs.AttendanceProto;
import com.service.writeprototohdfs.BuildingProto;
import com.service.writeprototohdfs.EmployeeProto;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.util.ToolRunner;

public class ProtoBuilder {

    private static final String EMPLOYEE_CSV_FILE_PATH =
            VariableSetter.CREATE_EMP_BUILDING_CSV_PATH + "employee1.csv";
    private static final String BUILDING_CSV_FILE_PATH =
            VariableSetter.CREATE_EMP_BUILDING_CSV_PATH + "building1.csv";

    private static final String EMPLOYEE_URI = "hdfs://localhost:8020/ProtoFiles/employee.seq";
    private static final String BUILDING_URI = "hdfs://localhost:8020/ProtoFiles/building.seq";
    private static final String ATTENDANCE_URI = "hdfs://localhost:8020/ProtoFiles/attendance.seq";

    public void buildTask(String[] args) throws Exception {
        protoTask();
        loadProtoToHbaseTask(args);
        joinTask(args);
    }

    private void protoTask() {
        EmployeeProto employee = new EmployeeProto(EMPLOYEE_CSV_FILE_PATH, EMPLOYEE_URI);
        BuildingProto building = new BuildingProto(BUILDING_CSV_FILE_PATH, BUILDING_URI);
        AttendanceProto attendance = new AttendanceProto(ATTENDANCE_URI);

        employee.set(); //load csv into employee proto
        building.set(); //load csv into building proto
        attendance.set(); //load csv into attendance proto

    }

    private void loadProtoToHbaseTask(String[] args) {
        BuildingHbaseDriver buildingHbaseDriver = new BuildingHbaseDriver();
        EmployeeHbaseDriver employeeHbaseDriver = new EmployeeHbaseDriver();

        try {
            //run job for loading building proto to Hbase
            ToolRunner.run(HBaseConfiguration.create(), buildingHbaseDriver, args);
            //run job for loading employee proto to Hbase
            ToolRunner.run(HBaseConfiguration.create(), employeeHbaseDriver, args);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void joinTask(String[] args) {
        EmployeePostProcessorDriver runJob = new EmployeePostProcessorDriver();
        AttendanceDriver attendanceJob = new AttendanceDriver();

        /*run job for joining employee with attendance to
            find the employee with the lowest attendance*/
        try {
            runJob.run(args);
            attendanceJob.run(args);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}


