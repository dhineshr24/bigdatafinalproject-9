package com.util;

import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;

import java.io.IOException;
import java.util.ArrayList;

public class HBaseTableCreator {
    private Connection connection;
    private String hbaseTableName;
    private ArrayList<String> columnFamilyList;

    public HBaseTableCreator(Connection connection, String hbaseTableNameToCreate, ArrayList<String> columnFamilyList) {
        this.connection = connection;
        this.hbaseTableName = hbaseTableNameToCreate;
        this.columnFamilyList = columnFamilyList;
    }

    //Function for creating an Hbase table
    public void CreateTable() throws IOException {
        Admin admin = connection.getAdmin();
        HTableDescriptor tableName = new HTableDescriptor(TableName.valueOf(hbaseTableName));

        //Adding column family in table
        for (String columnFam : columnFamilyList) {
            tableName.addFamily(new HColumnDescriptor(columnFam));
        }

        //create table if it dosen't exist
        if (!admin.tableExists(tableName.getTableName())) {
            System.out.print("Creating table. ");
            admin.createTable(tableName);
            System.out.println(" Done.");
        }
    }

}
