package com.util;

@FunctionalInterface
public interface ICsvCreator {
    void create(int noOfFilesToGenerate, String path, int noOfRowsToGenerate);
}
