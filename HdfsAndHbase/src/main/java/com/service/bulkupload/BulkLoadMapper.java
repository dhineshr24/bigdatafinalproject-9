package com.service.bulkupload;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class BulkLoadMapper extends Mapper<LongWritable, Text, ImmutableBytesWritable, Put> {

    private final byte[] CF_BYTES1 = Bytes.toBytes("personaldata");
    private final byte[] CF_BYTES2 = Bytes.toBytes("professionaldata");
    private final byte[][] QUAL_BYTES = {"name".getBytes(), "age".getBytes(), "phone_number".getBytes(),
            "company".getBytes(), "building_code".getBytes(), "address".getBytes()};

    @Override
    protected void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException {
        if (key.get() == 0 || value.getLength() == 0) {
            return;
        }
        String[] strArr = value.toString().split(",");
        byte[] rowKey = Bytes.toBytes(String.valueOf(key.get()));
        Put put = new Put(rowKey);
        //adding Column family and attributes in the table
        addColumn(put, strArr);
        context.write(new ImmutableBytesWritable(rowKey), put);
    }


    public void addColumn(Put put, String[] strArr) {
        for (int i = 0; i < strArr.length; i++) {
            String data = strArr[i];
            String colName = new String(QUAL_BYTES[i]);
            // if column name is company add it into the professional data
            if (colName.equalsIgnoreCase("company")) {
                put.addColumn(CF_BYTES2, QUAL_BYTES[i], Bytes.toBytes(data));
            } else {
                put.addColumn(CF_BYTES1, QUAL_BYTES[i], Bytes.toBytes(data));
            }
        }
    }
}


