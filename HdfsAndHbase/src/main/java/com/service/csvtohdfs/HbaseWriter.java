package com.service.csvtohdfs;

import com.config.HbaseConfiguration;
import com.util.HBaseTableCreator;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class HbaseWriter {
    public void load(FileStatus[] status, FileSystem fs, String hbaseTableNameToCreate,
                     ArrayList<String> columnFamilyList, ArrayList<String> columnAttributeList) {
        int rowid = 0;
        String rowname = "";
        BufferedReader br = null;
        Table table = null;
        try {
            Connection connection = HbaseConfiguration.HbaseConfiguration();
            //creating hbase table
            new HBaseTableCreator(connection, hbaseTableNameToCreate, columnFamilyList).CreateTable();
            for (int i = 0; i < status.length; i++) {
                //Step2. Load Csv file into Hbase
                br = new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
                String line = br.readLine();
                line = br.readLine();
                table = connection.getTable(TableName.valueOf(hbaseTableNameToCreate));//get table name
                while (line != null && line.length() != 0) {
                    StringTokenizer tokens = new StringTokenizer(line, ",");
                    rowname = String.valueOf(++rowid);
                    Put put = new Put(Bytes.toBytes((rowname)));
                    //adding Column family and attributes in the table
                    addColumn(columnFamilyList, columnAttributeList, put, tokens);
                    table.put(put);
                    line = br.readLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                //closing table and buffer reader objects
                br.close();
                table.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void addColumn(ArrayList<String> columnFamilyList,
                          ArrayList<String> columnAttributeList, Put put, StringTokenizer tokens) {
        for (int index = 0; index < columnAttributeList.size(); index++) {
            String colName = columnAttributeList.get(index);
            if (colName.equalsIgnoreCase("company")) {
                put.addColumn(Bytes.toBytes(columnFamilyList.get(1)),
                        Bytes.toBytes(colName), Bytes.toBytes(tokens.nextToken()));
            } else {
                put.addColumn(Bytes.toBytes(columnFamilyList.get(0)),
                        Bytes.toBytes(colName), Bytes.toBytes(tokens.nextToken()));
            }

        }
    }
}
