package com.service.wordcount;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class WordCountDriver {
    private String inPathFiles;
    private String jobName;
    private String outhPath;

    public WordCountDriver(String inPathFiles, String jobName, String outhPath) {
        this.inPathFiles = inPathFiles;
        this.jobName = jobName;
        this.outhPath = outhPath;
    }

    public int runJob() {
        Configuration c = new Configuration();
        Path input = new Path(inPathFiles);
        Path output = new Path(outhPath);
        Job job = null;
        boolean success = false;
        try {
            job = new Job(c, jobName);
            job.setJarByClass(WordCountDriver.class);
            job.setMapperClass(WordCountMapper.class); //set mapper class
            job.setReducerClass(WordCountReducer.class); //set reducer class
            job.setOutputKeyClass(Text.class); //set output key class
            job.setOutputValueClass(IntWritable.class); //set output value class
            FileInputFormat.addInputPath(job, input); //add input path
            FileOutputFormat.setOutputPath(job, output);
            success = job.waitForCompletion(true);
        } catch (IOException | InterruptedException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return success ? 0 : 1;

    }
}